$(document).ready(function () {

    /*NAVBAR LINKS HOVER DECORATION START*/

    $('.navbar-item-text').mouseenter(
        function () {
            $(this).css('color', '#ffffff').css('text-decoration', 'underline');
        }
    );

    $('.navbar-item-text').mouseleave(
        function () {

            if (!$(this).hasClass('active-link')) {
                $(this).css('text-decoration', 'none');
            }
        }
    );

    /*NAVBAR LINKS HOVER DECORATION END*/



    /*PARALLAX DIVS IMAGE SELECTION START*/

    $('.parallax-window').parallax({imageSrc: '/img/parallax1.jpg'});

    $('.parallax-window-2').parallax({imageSrc: '/img/parallax2.jpg'});

    /*PARALLAX DIVS IMAGE SELECTION END*/



    /*FILE INPUTS TRIGGERS START*/

    $('#file-input-1').on('click', function ($e) {
        $e.preventDefault();

        $('#file-input-button-1').trigger('click');

    });

    $('#file-input-2').on('click', function ($e) {
        $e.preventDefault();

        $('#file-input-button-2').trigger('click');
    });

    $('#file-input-3').on('click', function ($e) {
        $e.preventDefault();

        $('#file-input-button-3').trigger('click');
    });

    $('#dropbox-input-1').on('click', function ($e) {
        $e.preventDefault();

        Dropbox.choose({
            linkType: "direct",
            success: function (files) {
                $('#dropbox-input-button-1').val(files[0].link);
            }
        });
    });

    $('#dropbox-input-2').on('click', function ($e) {
        $e.preventDefault();

        Dropbox.choose({
            linkType: "direct",
            success: function (files) {
                $('#dropbox-input-button-2').val(files[0].link);
            }
        });
    });

    $('#dropbox-input-3').on('click', function ($e) {
        $e.preventDefault();

        Dropbox.choose({
            linkType: "direct",
            success: function (files) {
                $('#dropbox-input-button-3').val(files[0].link);
            }
        });
    });

    /*FILE INPUTS TRIGGERS END*/



    /*FORM SUBMIT AND VALIDATION START*/

    $('#submit-button').on('click', function ($e) {

        $e.preventDefault();

        var validationError = 1;

        /*VALIDATION ERRORS*/

        if ($('#first-name')[0].value === "") {

            $('#first-name').css('border-color', 'red');
            $('#first-name').css('color', 'red');

            validationError = validationError * 2;
        }

        if ($('#last-name')[0].value === "") {

            $('#last-name').css('border-color', 'red');
            $('#last-name').css('color', 'red');
            validationError = validationError * 2;
        }

        if ($('#city')[0].value === "") {

            $('#city').css('border-color', 'red');
            $('#city').css('color', 'red');
            validationError = validationError * 2;
        }

        if ($('#country')[0].value === "") {

            $('#country').css('border-color', 'red');
            $('#country').css('color', 'red');
            validationError = validationError * 2;
        }

        if ($('#email')[0].value === "") {

            $('#email').css('border-color', 'red');
            $('#email').css('color', 'red');
            validationError = validationError * 2;
        }

        if ($('#phone')[0].value === "") {

            $('#phone').css('border-color', 'red');
            $('#phone').css('color', 'red');
            validationError = validationError * 2;
        }

        if ($('#motivation')[0].value === "") {

            $('#motivation').css('border-color', 'red');
            $('#motivation').css('color', 'red');
            validationError = validationError * 2;
        }

        if ($('#file-input-button-1')[0].value === "" && $('#dropbox-input-button-1')[0].value === "") {

            $('#file-input-1').css('border-color', 'red');
            $('#file-input-1').css('color', 'rgba(255, 0, 0, 1)');
            $('#dropbox-input-1').css('border-color', 'red');
            $('#dropbox-input-1').css('color', 'rgba(255, 0, 0, 1)');
            validationError = validationError * 3;
        }

        /*VALIDATION ERRORS*/


        /*VALIDATION SUCCESS*/

        if ($('#first-name')[0].value !== "") {

            $('#first-name').css('border-color', 'transparent');
            $('#first-name').css('color', '#004475');
        }

        if ($('#last-name')[0].value !== "") {

            $('#last-name').css('border-color', 'transparent');
            $('#last-name').css('color', '#004475');
        }

        if ($('#city')[0].value !== "") {

            $('#city').css('border-color', 'transparent');
            $('#city').css('color', '#004475');
        }

        if ($('#country')[0].value !== "") {

            $('#country').css('border-color', 'transparent');
            $('#country').css('color', '#004475');
        }

        if ($('#email')[0].value !== "") {

            $('#email').css('border-color', 'transparent');
            $('#email').css('color', '#004475');
        }

        if ($('#phone')[0].value !== "") {

            $('#phone').css('border-color', 'transparent');
            $('#phone').css('color', '#004475');
        }

        if ($('#motivation')[0].value !== "") {

            $('#motivation').css('border-color', 'transparent');
            $('#motivation').css('color', '#004475');
        }

        if ($('#file-input-button-1')[0].value !== "" || $('#dropbox-input-button-1')[0].value !== "") {

            $('#file-input-1').css('border-color', 'transparent');
            $('#file-input-1').css('color', 'white');
            $('#dropbox-input-1').css('border-color', 'transparent');
            $('#dropbox-input-1').css('color', 'white');
        }

        /*VALIDATION SUCCESS*/




        if(validationError === 1) {

            console.log(validationError);
            sweetAlert("Form Submitted", "Form has successfully submitted!", "success");
        } else {
            console.log(validationError);
            return false;
        }
    });

    /*FORM SUBMIT AND VALIDATION END*/
});